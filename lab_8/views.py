from django.shortcuts import render

response={}
def index(request):
	html = 'lab_8/lab_8.html'
	response['author'] = "Ryan Naufal Pioscha"
	return render(request, html, response)	