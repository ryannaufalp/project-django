from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Ryan Naufal Pioscha' # TODO Implement this
current_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999,12,3)

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age' : calculate_age(birth_date.year)}
    return render(request, 'index_lab1.html', response)

# TODO Implement this to complete last checklist
def calculate_age(birth_year):
    return current_year - birth_year if birth_year <= current_year else 0