from django.db import models
from django.utils import timezone

class Todo(models.Model):
    title = models.CharField(max_length=27)
    description = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)

