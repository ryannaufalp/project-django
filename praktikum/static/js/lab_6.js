// Calculator
var erase = false;
var go = function(x) {
	var print = document.getElementById('print');
	
  if (x === 'ac') {
    print.value = "";
    erase = false;
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  }else if(x==='log'||x==='sin'||x==='tan'){
  	
  		if(x==='sin'||x==='tan'){print.value = eval('Math.'+x+'('+toDegrees(print.value)+')').toPrecision(2);}
		else{print.value = eval('Math.'+x+'10'+'('+print.value+')').toPrecision(2);}
  		erase = true;

  } else {
  	if(erase===true&&(x!==' * '&&x!==' - '&&x!==' + '&&x!==' / ')){
    	print.value = "";
	}
		print.value += x;
		erase = false;
  }
};

function toDegrees (angle) {
  return angle * Math.PI/180;
}

function evil(fn) {
  return new Function('return ' + fn)();
}




$(document).ready(function() {
    $(".chat-text").keypress(function(e) {
    
    if(e.which == 13) {
    	e.preventDefault(); 
    	var input = $("textarea").val(); 
		if(input.length > 1){
		  $("textarea").val(""); 
		  $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>'); 
		}
	}
	
	});

    $(".chat-button").click(function(){
	  var input = $("textarea").val();
      if(input.length > 1){
		  
		  $("textarea").val(""); 
		  $(".msg-insert").append('<p class="msg-send">'+input+'</p>'+'<br/>');
		}
    });
	
	var themes = [
	{"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
    {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
    {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
    {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
    {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
    {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
    {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
    {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
    {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
    {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
    {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
	];
	var selectedTheme = {"Indigo":{"bcgColor":"#3F51B5","fontColor":"#FAFAFA"}};
 
  if (localStorage.getItem("selectedTheme") === null) {
    localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
  }

  localStorage.setItem('themes', JSON.stringify(themes));
	
	var object = localStorage.getItem('themes');
	$('.my-select').select2({data: JSON.parse(object)});
	
  var selected = JSON.parse(localStorage.getItem('selectedTheme'));
    var key;
    var bcgColor;
    var fontColor;
	for (key in selected) {
    	if (selected.hasOwnProperty(key)) {
        	bcgColor=selected[key].bcgColor;
        	fontColor=selected[key].fontColor;
    	}
	}  
	$("body").css({"background-color": bcgColor});
	$("footer").css({"color":fontColor});


  
	$('.apply-button').on('click', function(){ 
    var valueId = $('.my-select').val();
    var a;
    var selectedTheme = {};
    for(a in themes){
    	if(a==valueId){
    		var bcgColor = themes[a].bcgColor;
    		var fontColor = themes[a].fontColor;
    		var text = themes[a].text;
    		$("body").css({"background-color": bcgColor});
			  $("footer").css({"color":fontColor});
    		selectedTheme[text] = {"bcgColor":bcgColor,"fontColor":fontColor};
    		localStorage.setItem('selectedTheme', JSON.stringify(selectedTheme));
    	}
    }
});
	
});
// END
